const Pool = require('pg').Pool
const pool = new Pool({
  user: process.env.POSTGRES_USER,
  host: process.env.POSTGRES_HOST,
  database: process.env.POSTGRES_DATABASE,
  password: process.env.POSTGRES_PASSWORD,
  port: 5432,
})

const getTodos = (request, response) => {
  pool.query('SELECT * FROM todos ORDER BY id ASC', (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  })
}

const getTodoById = (request, response) => {
  const id = parseInt(request.params.id)

  pool.query('SELECT * FROM todos WHERE id = $1', [id], (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  })
}

const createTodo = (request, response) => {
  const { text, marked } = request.body

  pool.query('INSERT INTO todos (text, marked) VALUES ($1, $2)', [text, marked], (error, result) => {
    if (error) {
      throw error
    }
    console.log(result)
    response.status(201).send(`Todo added`)
  })
}

const updateTodo = (request, response) => {
  const id = parseInt(request.params.id)
  const { text, marked } = request.body

  pool.query(
    'UPDATE todos SET text = $1, marked = $2 WHERE id = $3',
    [text, marked, id],
    (error, results) => {
      if (error) {
        throw error
      }
      response.status(200).send(`Todo modified with ID: ${id}`)
    }
  )
}

const deleteTodo = (request, response) => {
  const id = parseInt(request.params.id)

  pool.query('DELETE FROM todos WHERE id = $1', [id], (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).send(`Todo deleted with ID: ${id}`)
  })
}

module.exports = {
  getTodos,
  getTodoById,
  createTodo,
  updateTodo,
  deleteTodo,
}
