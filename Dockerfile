FROM node:10.15.0-alpine
RUN mkdir -p /api/
WORKDIR /api/
COPY yarn.lock /api/
COPY package*.json /api/
RUN yarn install
COPY . /api/
CMD ["yarn", "start"]