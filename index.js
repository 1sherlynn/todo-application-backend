const express = require('express')
var cors = require('cors')
const bodyParser = require('body-parser')
const app = express()
const port = process.env.PORT || 5000;
require('dotenv').config()

app.use(cors())
app.options('/todos/:id', cors()) 

const db = require('./queries')

app.use(bodyParser.json())
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
)

app.get('/', (request, response) => {
  response.json({ 
  	info: "Restful API for Sherlynn's Todo App",
  	backend: 'Node.js + Express',
  	database: 'PostgreSQL'
  	 })
})


console.log('print env variables',process.env.POSTGRES_USER);

app.get('/todos', cors(), db.getTodos)
app.get('/todos/:id', cors(), db.getTodoById)
app.post('/todos', cors(), db.createTodo)
app.put('/todos/:id', cors(), db.updateTodo)
app.delete('/todos/:id', cors(), db.deleteTodo)


app.listen(port, () => {
  console.log(`Application running on port ${port}.`)
})